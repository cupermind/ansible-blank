You can use this folder to keep some secrets. For example `aws.yml` can be placed
here with credentials specific only to this cluster.
