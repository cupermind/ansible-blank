All configuration comes to this directory.

Put vaults to files named `*.vault` there is `pre-commit git-hook` to check if
this file is encrypted before committing.

You can have here as many files as you want. Good idea can be to separate
configuration variables by purpose and place them to different files named
accordingly. For example, AWS specifics can go to files:

1. `aws.open` - for open configuration.
2. `aws.vault` - for secrets.
