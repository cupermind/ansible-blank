##  Ansible project: good start

> From authors of Cupermind.com

### Starring

1. `venv-ansible-playbook` - to have your Ansible version perfectly the same among all system updates.
2. `clusters` - to have multi-tier project (staging, productions, sandboxes and friends).
3. `.vautlfile` - to keep secrets secret.
4. `git pre-commit hook` - to commit only encrypted vaults.
5. `docs/` - check this dir for documentation.

### Plot

Just check this: http://cupermind.com/post/Ansible-Directories-Structure/

### Episode 1 - Ansible virtualenv

Thanks to [some people](https://www.ansible.com/blog/2013/12/08/the-origins-of-ansible), Ansible is a python project and it can be `virtualenved`, this way we don't have to use system Ansible from apt/yum/etc (Warning: avoid using Ansible from system package manager it can lead to unpredictable results).

### Episode 2 - Vaults

To not expose secrets in clear text files there is ansible-vault and to check that files were encrypted - there is `git pre-commit hook`, just name vault files `*.vault`.

### Episode 3 - AWS

You have several options to supply AWS credentials to prod:

1. `vars/aws.yml`- just include this file in playbooks.
2. `.aws-credentials` shell style - this file will be sourced by `venv-ansible-playbook` if exists.
3.  if you need different aws creds per cluster you can put creds to `clusters/somecluster/vars/aws.yml` and then use this: `include_vars: "{{ inventory_dir }}/vars/aws.yml"`

```
---
aws_access_key: "ACCESSKEY"
aws_secret_key: "SECRETKEY"

```
