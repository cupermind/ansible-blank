#!/bin/bash

PROJECT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VENV="$PROJECT_ROOT/.venv"
HOOK_SOURCE_DIR="git-hooks"
HOOK_DEST_DIR=".git/hooks"

CLUSTER=$1

if [ ! -d "$PROJECT_ROOT/clusters/$CLUSTER" ]; then
    echo "Cluster $CLUSTER not found"
    exit 1
fi

shift

# git hooks first
for SOURCE in $PROJECT_ROOT/${HOOK_SOURCE_DIR}/*; do
  SOURCE_NAME=$(basename ${SOURCE})
  DESTINATION="${PROJECT_ROOT}/${HOOK_DEST_DIR}/${SOURCE_NAME}"

  # If a hook of the same name already exists
  if [ -e ${DESTINATION} ]; then
    # If the existing hook is different
    if ! cmp -s ${SOURCE} ${DESTINATION}; then
      DESTINATION_BACKUP="${DESTINATION}_backup_${RUN_DATE}"
      # Back it up
      mv -v ${DESTINATION} ${DESTINATION_BACKUP}
      # Put new version in place
      cp -v ${SOURCE} ${DESTINATION}
      chmod 0755 ${DESTINATION}
    fi
  else
    # There were no hook in place
    cp -v ${SOURCE} ${DESTINATION}
    chmod 0755 ${DESTINATION}
  fi
done

# is the .venv is in place
if [ ! -e "$VENV" ]; then
    if [ `uname -s` == "Darwin" ]; then
        brew install python libffi openssl
        curl https://bootstrap.pypa.io/ez_setup.py -o - | python
        easy_install pip
        pip install virtualenv

    elif [ `uname -s` == "Linux" ]; then
        sudo apt-get install python-virtualenv python-dev libffi-dev libssl-dev
    fi

    virtualenv $VENV
    source $VENV/bin/activate
    pip install -r $PROJECT_ROOT/requirements.txt
    if [ $? -ne 0 ]; then
        rm -rf $VENV
        exit 1
    fi
fi

# and finally do some ansible stuff
if [ ! -z "$1" ]; then
    source $VENV/bin/activate
    if [ -e "$PROJECT_ROOT/.aws-credentials" ]; then
        source $PROJECT_ROOT/.aws-credentials
    fi
    $VENV/bin/ansible-playbook -i $PROJECT_ROOT/clusters/$CLUSTER/hosts $*
fi
