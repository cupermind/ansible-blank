## /vars

You can put here some global variables, for example `AWS creds`

### AWS creds

Example of `/vars/aws.yml`:


```
---
aws_access_key: "ACCESSKEY"
aws_secret_key: "SECRETKEY"

```
